Object= {}
function Object:new()
    return setmetatable({id=1},{__index=self})
end
function Object:createClass(className)
    _G[className] = {}
    setmetatable(_G[className],{__index=self})
    _G[className] = _G[className]:new()
end

-- 创建2个子类
Object:createClass('GameObject')
Object:createClass('GameObject2')

-- 创建子类属性，方法
GameObject.posX= 0;
GameObject.posY = 0;
function GameObject:Move()
    self.posX = self.posX + 1
    self.posY = self.posY + 1
    print(self.posX)
    print(self.posY)
end

GameObject:createClass('Player')

function Player:Move()
    print('重写')
end

print(Player.Move())
print(Player.id) -- 父类ID被访问到
print(Player.posX)
print(Player.posY)