---
--- Generated by EmmyLua(https://github.com/EmmyLua)
--- Created by WenWenC9.
--- DateTime: 2021-04-27 17:32
---

--function producer()
--    while true do
--        local x = io.read('*num') -- 生产一个新值
--        send(x) -- 发送给消费者 consumer
--    end
--end
--
--function consumer()
--    while true do
--        local x = receive() -- 接受从生产者
--        io.write(x, '\n') -- 使用这个新增
--    end
--end
print('---------------------------------------------------------------------------------------------------------------')
--function receive ()
--    local status, value = coroutine.resume(producer)
--    print(value)
--    print(status)
--    return value
--end
--
--function send (x)
--    coroutine.yield(x)
--end
--
--producer = coroutine.create(function()
--    print('生产者执行')
--    while true do
--        local x = io.read() -- produce new value
--        send(x)
--    end
--end)
--coroutine.resume(producer)


print('---------------------------------------------------------------------------------------------------------------')
----生产者消费者问题
--local newProductor
--
----生产
--function productor()
--    local i = 0
--    while true do
--        i = i + 1
--        send(i)             --将生产的物品发给消费者
--    end
--end
--
----消费
--function consumer()
--    while true do
--        local i = recieve()     --从生产者哪里拿到物品
--        print(i)
--    end
--end
--
----从生产者哪里得到物品的方法
--function recieve()
--    local status, value = coroutine.resume(newProductor)
--    return value
--end
--
----生产者把物品发送给消费者的方法
--function send(x)
--    coroutine.yield(x)      --生产者把值发送出去之后，就把自己的这个协同程序给挂起
--end
--
----启动程序
--newProductor = coroutine.create(productor)      --创建协同程序newProductor,（创建时执行productor()方法）
--consumer()

print('---------------------------------------------------------------------------------------------------------------')
function getNumber()
   local function getNumberHelper()
      co = coroutine.create(function ()
      coroutine.yield(1)
      coroutine.yield(2)
      coroutine.yield(3)
      coroutine.yield(4)
      coroutine.yield(5)
      end)
      return co
   end

   if(numberHelper) then
      status, number = coroutine.resume(numberHelper);

      if coroutine.status(numberHelper) == "dead" then
         numberHelper = getNumberHelper()
         status, number = coroutine.resume(numberHelper);
      end

      return number
   else
      numberHelper = getNumberHelper()
      status, number = coroutine.resume(numberHelper);
      return number
   end

end

for index = 1, 10 do
   print(index, getNumber())
end