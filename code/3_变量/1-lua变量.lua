--
-- Created by IntelliJ IDEA.
-- User: WenWenC9
-- Date: 2021-04-17
-- Time: 14:16
-- To change this template use File | Settings | File Templates.
--

a = 5
local b = 5

do
    local a = 6     -- 局部变量
    b = 6           -- 全局变量
    print(a,b);     --> 6 6
end
print(a,b)
