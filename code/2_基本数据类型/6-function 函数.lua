--
-- Created by IntelliJ IDEA.
-- User: WenWenC9
-- Date: 2021-04-17
-- Time: 13:47
-- To change this template use File | Settings | File Templates.
--

-- 函数
function f(n)
    if n == 0 then
        return 1
    else
        return n * f(n-1)
    end
end
print(f(1))
a = f
print(a(1))

-- 匿名函数
function a(tab, fun)
    for k, v in pairs(tab) do
        print(fun(k, v))
    end
end
tab = { key1 = "val1", key2 = "val2" }
a(tab, function(key, val)
    return key .. " = " .. val
end)