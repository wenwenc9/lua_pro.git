--
-- Created by IntelliJ IDEA.
-- User: WenWenC9
-- Date: 2021-04-17
-- Time: 14:03
-- To change this template use File | Settings | File Templates.
--

--[[
在 Lua 里，最主要的线程是协同程序（coroutine）。
它跟线程（thread）差不多，拥有自己独立的栈、局部变量和指令指针，可以跟其他协同程序共享全局变量和其他大部分东西。
--]]

