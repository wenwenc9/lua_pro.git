--
-- Created by IntelliJ IDEA.
-- User: WenWenC9
-- Date: 2021-04-17
-- Time: 12:23
-- To change this template use File | Settings | File Templates.
--


-- 双引号
string1 = "this is string1"
string2 = 'this is string2'

-- [[ ]]  表示 一块字符串
html = [[
<html>
<head></head>
<body>
    <a href="//www.w3cschool.cn/">w3cschoolW3Cschool教程</a>
</body>
</html>
]]
print(html)

-- 字符串运算
print("6" + 1)
print("6" + "6")
--print("a" + 2) -- 错误的

-- 拼接
print("a" .. "b")
print(157 .. 428)

-- 计算字符串长度
a = 'www.baidu.com'
print(#a)