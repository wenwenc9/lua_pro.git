--
-- Created by IntelliJ IDEA.
-- User: WenWenC9
-- Date: 2021-04-17
-- Time: 12:37
-- To change this template use File | Settings | File Templates.
--

-- 创建空表
tab1 ={}

-- 初始化表
tab2 = {'apple','pear','orange','grape'}

-- 表的键值
a = {}
a['key'] = 'value'
key = 10
a[key] = 22
a[key] = a[key] + 11
for k,v in pairs(a) do
    print(k .. ":" .. v)
end

-- 表的索引
tab1 = {'apple','orange','pear','grape'}
for k,v in pairs(tab1) do
    print(k .. ':' .. v)
end
print(tab1[2])

-- 表的长度
a3 = {}
for i = 1, 10 do
    a3[i] = i
end
a3["key"] = "val"
print(a3["key"])
print(a3["none"])
print(a3['1'])
print(a3[1])